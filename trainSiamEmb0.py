# -*- coding: utf-8 -*-

"""
El siguiente codigo se realizo estudiando y retomando parte de las siguientes fuentes

Fernando-López, Gibrán-Fuentes and Fabian-Garcia
Authorship Verification from Texts through Convolutional and Recurrent Neural Networks
https://github.com/FernandoLpz/AuthorVerificiation


"""

from __future__ import print_function
import os
import argparse
import pickle
import pandas as pd
import numpy as np

from tensorflow.keras import backend as K

from tensorflow.keras.models import Model, Sequential, load_model
from tensorflow.keras.layers import Input, Conv1D, MaxPooling1D, Lambda, LSTM, Dropout, BatchNormalization, Activation, Dense

import tensorflow as tf
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping
from keras_radam.training import RAdamOptimizer

from tensorflow.python.client import device_lib
print(device_lib.list_local_devices())

#dtype='float16'
#K.set_floatx(dtype)
#K.set_epsilon(1e-4) 

def save_obj(obj, name ):
    with open( name , 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(name ):
    with open( name , 'rb') as f:
        return pickle.load(f)

#def SiameseArquitecture2(longitud, dimension):
#    model = Sequential(  )
#    model.add(Conv1D(60, 30, input_shape=(longitud, dimension)))
##    model.add(BatchNormalization())
##    model.add(Activation('relu'))#preactivacion ORIGINALs
#    
#    #model.add(Activation('relu'))#preactivacion Optimizacion
#    #model.add(BatchNormalization())
#    
#    #model.add(Dropout(0.1))
#    #lay = Conv1D(5, 16)
#    #model.add( lay )
#    
#    model.add(Activation('relu'))
#    model.add(BatchNormalization())
#    model.add(Dropout(0.1))
#    
#    model.add( Dense(100) )
#    model.add(Activation('relu'))
#    
#    #model.add(MaxPooling1D(2)) # antes 4
#    
#    #lay = LSTM(80, recurrent_dropout=0.1, return_sequences=False)
#    model.add(CudnnLSTM(80, recurrent_dropout=0.1, return_sequences=False) )
#
#    model.add(Activation('relu'))
#	
#    model.summary()
#    return model

def SiameseArquitecture(longitud, dimension):
    model = Sequential()
    model.add(Conv1D(76, 12, input_shape=(longitud, dimension)))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Dropout(0.1))
    model.add(Conv1D(50, 12))
    model.add(Activation('relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.1))   
    model.add(MaxPooling1D(4))
    lay = LSTM(64, recurrent_dropout=0.1, return_sequences=False)
    model.add(lay)
    model.add(Activation('relu'))
    
    model.summary()
    return model

cero = np.zeros( (1,300) )
dd1 = load_obj('dicC2.pkl')
def char2emb(xx):
    res0 = np.zeros( (LTEXT,300) ,dtype='float32' )
    for i,x in enumerate(xx):
        #print(dd[x])
        res0[i] = dd1.get(x,cero)
    return res0
#def char2emb(xx,dd,L):
#    #print(len(dd),len(dd[0]))
#    res0 = np.zeros( (L,300) )
#    lt = len(xx)
#    for i in range(L):
#        #print(dd[x])
#        if i < lt:
#            res0[i] = dd.get(xx[i],0)
#        else:
#            res0[i] = 0
#    return res0
@tf.function
def parse_fn(string):
    res = tf.py_function( # correccion del arreglo [0]
                    func=char2emb, inp=[string] , Tout=[tf.float32]
                    )
    #res
    return res


parser = argparse.ArgumentParser()
parser = argparse.ArgumentParser(description='PAN-19 Baseline Authorship Attribution Method')
parser.add_argument('-i', type=str,default="../pan19-cross-domain-authorship-attribution-training-dataset-2019-01-23/", help='Path to the main folder of a collection of attribution problems')
parser.add_argument('-o', type=str,default="../", help='Path to an output folder')
parser.add_argument('-L', type=str, default=800, help='LT')
parser.add_argument('-BT', type=int, default=10, help='BT')
parser.add_argument('-EP', type=int, default=4000, help='EP')
parser.add_argument('-W', type=str, default= None, help='EP')

#parser.add_argument('-DATA', type=str, default= '../../', help='EP')
parser.add_argument('-DATA', type=str, default= '/mnt/sx82T1/CdataEmb/', help='data PATH')


args = parser.parse_args()

Nthreads = (int)(os.popen('grep -c cores /proc/cpuinfo').read()) 

if not args.i:
    print('ERROR: The input folder is required')
    parser.exit(1)
if not args.o:
    print('ERROR: The output folder is required')
    parser.exit(1)

# MACROS
LTEXT = int(args.L)
print('Loading PreConfig')
database = 'RawText_L'+str(args.L)


dd1 = {'holi':2134}

dataT,lab,dicClas = load_obj(args.DATA+database+'dataTdata.plk')
#in1, in2 = dataT
#in1 = tf.data.Dataset.from_tensor_slices( in1 )
#in2 = tf.data.Dataset.from_tensor_slices( in2 )

#in1 = in1.map(map_func= lambda x :tf.py_function(func=char2emb,inp=[x],Tout=[tf.float32]), num_parallel_calls=tf.data.experimental.AUTOTUNE)
#in1.output_classes == (tf.Tensor)
#in1.output_types == (tf.float32)
#in1.output_shapes == ([args.L,300])

#in2 = in2.map(map_func= lambda x :tf.py_function(func=char2emb,inp=[x],Tout=[tf.float32]), num_parallel_calls=tf.data.experimental.AUTOTUNE)
#in2.output_classes == (tf.Tensor)
#in2.output_types == (tf.float32)
#in2.output_shapes == ([args.L,300])

#dataset = in1

longitud = LTEXT
dimension = 300


model = None
if args.W != None:
    print('loading W : ', args.W)
    #model.load_weights( args.W ) 
    model = load_model( args.W )
else:
    print('Cargando red siamesa')
    Siamese = SiameseArquitecture( longitud,dimension )

    print('Generando entradas de la red siamesa')
    input1 = Input(shape=( longitud,dimension   ) )
    input2 = Input(shape=( longitud,dimension   ) )
    
    print('Procesamiento de entradas')
    brenchLeft = Siamese(input1)
    brenchRight = Siamese(input2)
    
    print('Generando Metrica')
    #L1_distance = lambda x: K.abs(x[0]-x[1])
    #pred = Lambda(euclidean_distance, output_shape=eucl_dist_output_shape )([brenchLeft, brenchRight])
    # https://www.kaggle.com/c/quora-question-pairs/discussion/33631
    #both = concatenate( [brenchLeft, brenchRight] ,axis = 1 )
    
    sub = tf.keras.layers.Subtract()([brenchLeft, brenchRight])
    both = K.abs(sub)
    bothDr = Dropout(.2)(both)
    bothDe1 = Dense( 512 , activation='relu' )( bothDr )
    bothBt = BatchNormalization()(bothDe1)
    bothDe2 = Dense( 256 , activation='relu' )( bothBt )
    prediction = Dense( 1 , activation='tanh' )( bothDe2 )
    model = Model([ input1 , input2 ] , prediction)



print("Comenzando entrenamiento: ")
# checkpoint
#filepath="{epoch:03d}-{val_loss:.4f}.hdf5"
modName = 'models/SE_CP_LEN'+str(args.L)+'L{loss:.4f}VL{val_loss:.4f}EP{epoch:04d}BT'+str(args.BT)

checkpoint = ModelCheckpoint(modName+'.h5', monitor='val_loss', verbose=2, save_best_only=True,
                              save_weights_only=False,
                             mode='min'
                             )
early_stop = EarlyStopping(monitor='val_loss', verbose=1, patience=200 , mode = 'min')
#hwm = []
#hw1 = historyW.historyW(model,hwm)

callbacks_list = [early_stop, checkpoint ]

radam = RAdamOptimizer(1e-3 )
#radam = RAdam(1e-4)
model.compile(optimizer=radam , loss = 'binary_crossentropy' )

history = model.fit( dataT , lab ,batch_size = 40,
                         #validation_data=valData,
                         validation_split = .15,
                         callbacks=callbacks_list,
                         verbose=2,
                         shuffle=True, 
                         epochs=args.EP )
print('entrenamiento terminado')

#    history = model.fit([XtrainLeft, XtrainRigth], Ytrain,
#                        validation_data=([XtestLeft, XtestRigth],Ytest), 
#                        epochs=EP, batch_size=BT)
#   
# Generators
#training_generator = dt.DataGenerator(partition['train'], train_labels, **paramsT)
#validation_generator = dt.DataGenerator(partition['validation'], test_labels, **paramsV)
#
#
#history = model.fit_generator(generator=training_generator,
#                     validation_data=validation_generator,
#                     use_multiprocessing=False,
#                     workers=Nthreads-1,
#                     callbacks=callbacks_list,
#                     verbose=1,
#                     shuffle=True, 
#                     epochs=EP)
#




