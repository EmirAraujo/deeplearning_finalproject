# -*- coding: utf-8 -*-

"""
El siguiente codigo se realizo estudiando y retomando parte de las siguientes fuentes

Fernando-López, Gibrán-Fuentes and Fabian-Garcia
Authorship Verification from Texts through Convolutional and Recurrent Neural Networks
https://github.com/FernandoLpz/AuthorVerificiation


"""

from __future__ import print_function
import os
import argparse
import pickle
import pandas as pd
import numpy as np

from tensorflow.keras import backend as K

from tensorflow.keras.models import Model, Sequential, load_model
from tensorflow.keras.layers import Input, Conv1D, MaxPooling1D, Lambda, LSTM, Dropout, BatchNormalization, Activation, Dense, Embedding

import tensorflow as tf
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping
from keras_radam.training import RAdamOptimizer

from tensorflow.python.client import device_lib
print(device_lib.list_local_devices())

#dtype='float16'
#K.set_floatx(dtype)
#K.set_epsilon(1e-4) 

def save_obj(obj, name ):
    with open( name , 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(name ):
    with open( name , 'rb') as f:
        return pickle.load(f)

def euclidean_distance(vects):
	x, y = vects
	return K.sqrt(K.maximum(K.sum(K.square(x - y), axis=1, keepdims=True), K.epsilon()))

def eucl_dist_output_shape(shapes):
	shape1, shape2 = shapes	
	return (shape1[0], 1)

def SiameseArquitecture(longitud, dimension):
    model = Sequential()
    model.add(Conv1D(76, 12, input_shape=(longitud, dimension)))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Dropout(0.1))
    model.add(Conv1D(50, 12))
    model.add(Activation('relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.1))   
    model.add(MaxPooling1D(4))
    lay = LSTM(64, recurrent_dropout=0.1, return_sequences=False)
    model.add(lay)
    model.add(Activation('relu'))
    
    model.summary()
    return model


def contrastive_loss(y_true, y_pred):
    '''Contrastive loss from Hadsell-et-al.'06
    http://yann.lecun.com/exdb/publis/pdf/hadsell-chopra-lecun-06.pdf
    '''
    margin = 1
    square_pred = K.square(y_pred)
    margin_square = K.square(K.maximum(margin - y_pred, 0))
    return K.mean(y_true * square_pred + (1 - y_true) * margin_square)



parser = argparse.ArgumentParser()
parser = argparse.ArgumentParser(description='PAN-19 Baseline Authorship Attribution Method')
parser.add_argument('-i', type=str,default="../pan19-cross-domain-authorship-attribution-training-dataset-2019-01-23/", help='Path to the main folder of a collection of attribution problems')
parser.add_argument('-o', type=str,default="../", help='Path to an output folder')
parser.add_argument('-L', type=str, default=1000, help='LT')
parser.add_argument('-BT', type=int, default=200, help='BT')
parser.add_argument('-EP', type=int, default=4000, help='EP')
parser.add_argument('-W', type=str, default= None, help='EP')

#parser.add_argument('-DATA', type=str, default= '../../', help='EP')
parser.add_argument('-DATA', type=str, default= '/mnt/sx82T1/CdataEmb/', help='data PATH')


args = parser.parse_args()

Nthreads = (int)(os.popen('grep -c cores /proc/cpuinfo').read()) 

if not args.i:
    print('ERROR: The input folder is required')
    parser.exit(1)
if not args.o:
    print('ERROR: The output folder is required')
    parser.exit(1)

# MACROS
LTEXT = int(args.L)
print('Loading PreConfig')
database = 'RawText_L'+str(args.L)

alphabet = load_obj(args.DATA+database+'alphabet.plk')
dd1 = load_obj('dicC2.pkl')

vocabSize = len(alphabet) + 1
weights_embedings = np.zeros( (vocabSize , 300) )
for i in range(len(alphabet)):
    weights_embedings[i] = dd1[alphabet[i]]

#dd1 = {'holi':2134}

dataT,lab,dicClas = load_obj(args.DATA+database+'dataTdata.plk')
print(dataT[0])

#in1, in2 = dataT
#in1 = tf.data.Dataset.from_tensor_slices( in1 )
#in2 = tf.data.Dataset.from_tensor_slices( in2 )

#in1 = in1.map(map_func= lambda x :tf.py_function(func=char2emb,inp=[x],Tout=[tf.float32]), num_parallel_calls=tf.data.experimental.AUTOTUNE)
#in1.output_classes == (tf.Tensor)
#in1.output_types == (tf.float32)
#in1.output_shapes == ([args.L,300])

#in2 = in2.map(map_func= lambda x :tf.py_function(func=char2emb,inp=[x],Tout=[tf.float32]), num_parallel_calls=tf.data.experimental.AUTOTUNE)
#in2.output_classes == (tf.Tensor)
#in2.output_types == (tf.float32)
#in2.output_shapes == ([args.L,300])

#dataset = in1

longitud = LTEXT
dimension = 300


model = None
if args.W != None:
    print('loading W : ', args.W)
    #model.load_weights( args.W ) 
    model = load_model( args.W )
else:
    print('Cargando red siamesa')
    Siamese = SiameseArquitecture( longitud,dimension )

    print('Generando entradas de la red siamesa')
    in1 = Input(shape=( longitud*2, )  )
    x1 ,x2 = tf.split(in1, num_or_size_splits=2, axis=1 )
    emb = Embedding( vocabSize , dimension, input_length=x1.shape[-1] , weights=[weights_embedings] , trainable=False)
    x1emb = emb(x1)
    x2emb = emb(x2)
    #input2 = Input(shape=( longitud,dimension   ) )
    
    print('Procesamiento de entradas')
    brenchLeft = Siamese(x1emb)
    brenchRight = Siamese(x2emb)
    
    print('Generando Metrica')
    distance = Lambda(euclidean_distance, output_shape=eucl_dist_output_shape)([brenchLeft, brenchRight])
    
    model = Model( in1  , distance)


print("Comenzando entrenamiento: ")

modName = 'models/SED_CP_LEN'+str(args.L)+'L{loss:.4f}VL{val_loss:.4f}EP{epoch:04d}BT'+str(args.BT)

checkpoint = ModelCheckpoint(modName+'.h5', monitor='val_loss', verbose=2, save_best_only=True,
                              save_weights_only=False,
                             mode='min'
                             )
early_stop = EarlyStopping(monitor='val_loss', verbose=1, patience=200 , mode = 'min')
#hwm = []
#hw1 = historyW.historyW(model,hwm)

callbacks_list = [early_stop, checkpoint ]

radam = RAdamOptimizer(1e-3 )
#radam = RAdam(1e-4)
model.compile(optimizer=radam , loss = contrastive_loss )
model.summary()


history = model.fit( dataT , lab ,batch_size = args.BT,
                         #validation_data=valData,
                         validation_split = .20,
                         callbacks=callbacks_list,
                         verbose=2,
                         shuffle=True, 
                         epochs=args.EP )
print('entrenamiento terminado')

#    history = model.fit([XtrainLeft, XtrainRigth], Ytrain,
#                        validation_data=([XtestLeft, XtestRigth],Ytest), 
#                        epochs=EP, batch_size=BT)
#   
# Generators
#training_generator = dt.DataGenerator(partition['train'], train_labels, **paramsT)
#validation_generator = dt.DataGenerator(partition['validation'], test_labels, **paramsV)
#
#
#history = model.fit_generator(generator=training_generator,
#                     validation_data=validation_generator,
#                     use_multiprocessing=False,
#                     workers=Nthreads-1,
#                     callbacks=callbacks_list,
#                     verbose=1,
#                     shuffle=True, 
#                     epochs=EP)
#




